#
# Be sure to run `pod lib lint DGConverse.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'DGConverse'
  s.version          = '0.1.11'
  s.summary          = 'A complete operation-based iOS networking framework.'
  s.description      = 'The Network Layer uses NSOperations as wrapper for each particular a network call (data, upload or download). Same for other tasks as caching and data conversion, which now are completely independent operations; facilitating their reuse of in other projects.'
  s.homepage         = 'https://gitlab.com/danielgaston/dgconverse.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Daniel Gaston' => 'daniel.gaston.iglesias@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/danielgaston/dgconverse.git', :tag => s.version.to_s }
  s.requires_arc = true
  s.ios.deployment_target = '11.0'

  s.source_files = 'DGConverse/**/*.{h,m}', 'SupportingFiles/**/*.{h}', 'Framework/**/*.{h}'
  s.prefix_header_contents = '#import "DGConverse.h"'

  s.dependency 'JSONModel'
  s.dependency 'SPTPersistentCache'
  
end
